// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final void Function()? onTap;
  final String text;
  final double height;
  final double width;
  final bool state;
  const CustomButton({
    Key? key,
    this.onTap,
    required this.text,
    this.height = 42,
    this.width = double.infinity,
    this.state = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(16),
      child: Ink(
        decoration: BoxDecoration(
          color: state ? Colors.redAccent : Colors.grey.shade300,
          borderRadius: BorderRadius.circular(16),
        ),
        height: height,
        width: width,
        child: Center(
            child: Text(
          text,
          style: TextStyle(
            fontSize: 17,
            color: state ? Colors.white : Colors.grey.shade700,
          ),
        )),
      ),
    );
  }
}
