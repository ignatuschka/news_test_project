// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/usecase/delete_article_by_id.dart';
import 'package:news/domain/usecase/insert_article_usecase.dart';
import 'package:news/navigation/news_navigator.dart';
import 'package:news/presentation/widgets/article_list_item/bloc/article_list_item_event.dart';
import 'package:news/presentation/widgets/article_list_item/bloc/article_list_item_state.dart';

class ArticleListItemBloc extends Bloc<ArticleListItemEvent, ArticleListItemState> {
  final NewsNavigator navigator;
  final InsertArticleUsecase insertArticleUsecase;
  final DeleteArticleByIdUsecase deleteArticleByIdUsecase;
  final ArticlesDbEntity article;
  ArticleListItemBloc({
    required this.navigator,
    required this.insertArticleUsecase,
    required this.deleteArticleByIdUsecase,
    required this.article,
  }) : super(ArticleListItemState.initial()) {
    on<ArticleListItemInitialEvent>(_initial);
    on<InsertOrDeleteEvent>(_insertOrDeleteArticle);
    on<NavigateChosenNewsEvent>(_navigateChosenNews);
    add(ArticleListItemInitialEvent());
  }

  Future<void> _initial(ArticleListItemEvent event, Emitter<ArticleListItemState> emit) async {
    emit(state.setItemArticle(ArticlesDbEntity(
      id: article.id,
      title: article.title ?? 'Название новости',
      description: article.description ?? 'Описание новости',
      urlToImage: article.urlToImage ?? '',
      favorited: article.favorited,
    )));
  }

  Future<void> _insertOrDeleteArticle(ArticleListItemEvent event, Emitter<ArticleListItemState> emit) async {
    emit(state.setIsStarLoading(true));
    if (state.itemArticle.favorited) {
      await deleteArticleByIdUsecase(state.itemArticle.id!);
      emit(state
          .setItemArticle(ArticlesDbEntity(
            title: state.itemArticle.title,
            description: state.itemArticle.description,
            urlToImage: state.itemArticle.urlToImage,
            favorited: false,
          ))
          .setIsStarLoading(false));
    } else {
      final id = await insertArticleUsecase(state.itemArticle);
      emit(state
          .setItemArticle(ArticlesDbEntity(
            id: id,
            title: state.itemArticle.title,
            description: state.itemArticle.description,
            urlToImage: state.itemArticle.urlToImage,
            favorited: true,
          ))
          .setIsStarLoading(false));
    }
  }

  Future<void> _navigateChosenNews(ArticleListItemEvent event, Emitter<ArticleListItemState> emit) async {
    navigator.navigateToChosenNews(article: state.itemArticle);
  }
}
