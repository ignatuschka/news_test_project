import 'package:built_value/built_value.dart';
import 'package:news/domain/entity/articles_db_entity.dart';

part 'article_list_item_state.g.dart';

abstract class ArticleListItemState implements Built<ArticleListItemState, ArticleListItemStateBuilder> {
  bool get isStarLoading;
  ArticlesDbEntity get itemArticle;

  ArticleListItemState._();

  factory ArticleListItemState([void Function(ArticleListItemStateBuilder)? updates]) = _$ArticleListItemState;

  factory ArticleListItemState.initial() => ArticleListItemState((b) => b
    ..isStarLoading = false
    ..itemArticle = const ArticlesDbEntity(
      title: '',
      description: '',
      urlToImage: '',
      favorited: false,
    ));

  ArticleListItemState setIsStarLoading(bool isStarLoading) => rebuild((b) => (b)..isStarLoading = isStarLoading);

  ArticleListItemState setItemArticle(ArticlesDbEntity itemArticle) => rebuild((b) => (b)..itemArticle = itemArticle);
}
