import 'package:news/domain/entity/articles_db_entity.dart';

abstract class ArticleListItemEvent {}

class ArticleListItemInitialEvent extends ArticleListItemEvent {}

class NavigateChosenNewsEvent extends ArticleListItemEvent {
  final ArticlesDbEntity article;
  NavigateChosenNewsEvent({required this.article});
}

class InsertOrDeleteEvent extends ArticleListItemEvent {}
