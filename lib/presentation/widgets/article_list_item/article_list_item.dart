import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news/presentation/widgets/article_list_item/bloc/article_list_item_bloc.dart';
import 'package:news/presentation/widgets/article_list_item/bloc/article_list_item_event.dart';
import 'package:news/presentation/widgets/article_list_item/bloc/article_list_item_state.dart';

class ArticleListItem extends StatelessWidget {
  const ArticleListItem({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ArticleListItemBloc, ArticleListItemState>(
      builder: (context, state) {
        return Column(
          children: [
            ListTile(
              visualDensity: const VisualDensity(vertical: 4),
              titleAlignment: ListTileTitleAlignment.top,
              leading: (state.itemArticle.urlToImage!.isNotEmpty)
                  ? CachedNetworkImage(
                      errorWidget: (context, url, error) => Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Image.asset(
                          'assets/images/errorImage.png',
                          height: 80,
                          width: 80,
                        ),
                      ),
                      imageBuilder: (context, imageProvider) => Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      imageUrl: state.itemArticle.urlToImage!,
                      width: 80,
                      height: 80,
                      fit: BoxFit.fill,
                    )
                  : Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Image.asset(
                        'assets/images/errorImage.png',
                        height: 80,
                        width: 80,
                        fit: BoxFit.fill,
                      ),
                    ),
              title: (state.itemArticle.title != null)
                  ? Text(
                      state.itemArticle.title!,
                      maxLines: 5,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(fontSize: 17),
                    )
                  : const Text(
                      'Название новости',
                      style: TextStyle(fontSize: 17),
                    ),
              subtitle: (state.itemArticle.description != null)
                  ? Text(
                      state.itemArticle.description!,
                      maxLines: 5,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(fontSize: 14),
                    )
                  : const Text(
                      'Описание новости',
                      style: TextStyle(fontSize: 14),
                    ),
              trailing: IconButton(
                onPressed: () => context.read<ArticleListItemBloc>().add(InsertOrDeleteEvent()),
                icon: Icon(
                  Icons.star,
                  color: state.itemArticle.favorited ? Colors.redAccent : Colors.grey.shade200,
                ),
              ),
              onTap: () => context.read<ArticleListItemBloc>().add(NavigateChosenNewsEvent(article: state.itemArticle)),
            ),
            const Divider(),
          ],
        );
      },
    );
  }
}
