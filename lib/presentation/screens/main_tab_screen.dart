import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:news/navigation/app_router.dart';
import 'package:news/presentation/widgets/custom_button.dart';

class MainTabPage extends StatelessWidget {
  const MainTabPage({super.key});

  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter.tabBar(
      routes: const [
        TopHeadlinesRoute(),
        EverythingRoute(),
      ],
      builder: (context, child, controller) {
        return Scaffold(
            appBar: AppBar(
              leading: const AutoLeadingButton(),
              title: Text(
                controller.index == 0 ? 'Top Headlines' : 'Everything',
                style: const TextStyle(fontSize: 17),
              ),
              centerTitle: true,
              bottom: TabBar(
                tabs: [
                  CustomButton(
                    state: controller.index == 0,
                    text: 'Главная',
                  ),
                  CustomButton(
                    state: controller.index == 1,
                    text: 'Все новости',
                  )
                ],
                controller: controller,
                splashFactory: NoSplash.splashFactory,
                indicator: const ShapeDecoration(shape: LinearBorder.none),
                dividerColor: Colors.transparent,
              ),
            ),
          body: Padding(
            padding: const EdgeInsets.only(top: 32),
            child: child,
          ),
        );
      },
    );
  }
}
