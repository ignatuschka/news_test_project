import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news/di/news_di.dart';
import 'package:news/presentation/screens/everything/bloc/everything_bloc.dart';
import 'package:news/presentation/screens/everything/bloc/everything_event.dart';
import 'package:news/presentation/screens/everything/bloc/everything_state.dart';

class EverythingScreen extends StatefulWidget {
  const EverythingScreen({super.key});

  @override
  State<EverythingScreen> createState() => _EverythingPage();
}

class _EverythingPage extends State<EverythingScreen> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_loadMoreData);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _loadMoreData() {
    if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
      context.read<EverythingBloc>().add(PaginationEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EverythingBloc, EverythingState>(
      builder: (context, state) {
        if (!state.isLoading) {
          return RefreshIndicator(
            onRefresh: () async => context.read<EverythingBloc>().add(EverythingInitialEvent()),
            child: ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              shrinkWrap: true,
              controller: _scrollController,
              itemCount: state.news.length + 1,
              itemBuilder: (context, index) {
                if (index < state.news.length || index >= state.totalResults) {
                  return ArticleItem(article: state.news[index]);
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
