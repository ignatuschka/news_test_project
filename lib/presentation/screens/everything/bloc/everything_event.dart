abstract class EverythingEvent {}

class EverythingInitialEvent extends EverythingEvent {}

class PaginationEvent extends EverythingEvent {}
