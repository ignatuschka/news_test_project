import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/usecase/all_articles_usecase.dart';
import 'package:news/domain/usecase/everything_usecase.dart';
import 'package:news/domain/usecase/news_params.dart';
import 'package:news/presentation/screens/everything/bloc/everything_event.dart';
import 'package:news/presentation/screens/everything/bloc/everything_state.dart';

class EverythingBloc extends Bloc<EverythingEvent, EverythingState> {
  final EverythingUsecase everythingUsecase;
  final AllArticlesUsecase allArticlesUsecase;
  EverythingBloc({
    required this.everythingUsecase,
    required this.allArticlesUsecase,
  }) : super(EverythingState.initial()) {
    on<EverythingInitialEvent>(_initial);
    on<PaginationEvent>(_paginationEvent);
    add(EverythingInitialEvent());
  }

  Future<void> _initial(EverythingEvent event, Emitter<EverythingState> emit) async {
    emit(state.setIsLoading(true));
    final networkResult = await everythingUsecase(NewsParams(page: 1, pageSize: 15));
    final localResult = await allArticlesUsecase(EmptyParams());
    final articles = networkResult.articles.map((e) {
        if (localResult.contains(ArticlesDbEntity(
          title: e.title,
          description: e.description,
          urlToImage: e.urlToImage,
          favorited: true,
        ))) {
          return ArticlesDbEntity(
            id: localResult
                .singleWhere((element) =>
                    element ==
                    ArticlesDbEntity(
                      title: e.title,
                      description: e.description,
                      urlToImage: e.urlToImage,
                      favorited: true,
                    ))
                .id,
            title: e.title,
            description: e.description,
            urlToImage: e.urlToImage,
            favorited: true,
          );
        } else {
          return ArticlesDbEntity(
            title: e.title,
            description: e.description,
            urlToImage: e.urlToImage,
            favorited: false,
          );
        }
      }).toList();
    emit(state.setIsLoading(false).setNews(articles).setTotalResults(networkResult.totalResults));
  }

  Future<void> _paginationEvent(EverythingEvent event, Emitter<EverythingState> emit) async {
    if (state.totalResults / state.page > 14) {
      emit(state.setPage(state.page + 1));
      final networkResult = await everythingUsecase(NewsParams(page: state.page, pageSize: 15));
      final localResult = await allArticlesUsecase(EmptyParams());
      final articles = networkResult.articles.map((e) {
        if (localResult.contains(ArticlesDbEntity(
          title: e.title,
          description: e.description,
          urlToImage: e.urlToImage,
          favorited: true,
        ))) {
          return ArticlesDbEntity(
            id: localResult
                .singleWhere((element) =>
                    element ==
                    ArticlesDbEntity(
                      title: e.title,
                      description: e.description,
                      urlToImage: e.urlToImage,
                      favorited: true,
                    ))
                .id,
            title: e.title,
            description: e.description,
            urlToImage: e.urlToImage,
            favorited: true,
          );
        } else {
          return ArticlesDbEntity(
            title: e.title,
            description: e.description,
            urlToImage: e.urlToImage,
            favorited: false,
          );
        }
      }).toList();
      emit(state.setNews(state.news + articles));
    }
  }
}
