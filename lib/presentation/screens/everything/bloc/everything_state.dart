import 'package:built_value/built_value.dart';
import 'package:news/domain/entity/articles_db_entity.dart';

part 'everything_state.g.dart';

abstract class EverythingState implements Built<EverythingState, EverythingStateBuilder> {
  bool get isLoading;
  List<ArticlesDbEntity> get news;
  int get page;
  int get totalResults;

  EverythingState._();

  factory EverythingState([void Function(EverythingStateBuilder)? updates]) = _$EverythingState;

  factory EverythingState.initial() => EverythingState((b) => b
    ..isLoading = true
    ..news = []
    ..page = 1
    ..totalResults = 0);

  EverythingState setIsLoading(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  EverythingState setNews(List<ArticlesDbEntity> news) => rebuild((b) => (b)..news = news);
  
  EverythingState setPage(int page) => rebuild((b) => (b)..page = page);

  EverythingState setTotalResults(int totalResults) => rebuild((b) => (b)..totalResults = totalResults);
}
