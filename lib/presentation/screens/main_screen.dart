import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:news/navigation/app_router.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter(
      routes: const [
        MainTabRoute(),
        FavoritesRoute(),
      ],
      builder: (context, child, controller) {
        final tabsRouter = AutoTabsRouter.of(context);
        return Scaffold(
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: child,
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: [
              BottomNavigationBarItem(
                  icon: SvgPicture.asset('assets/icons/category1.svg'),
                  activeIcon: SvgPicture.asset(
                    'assets/icons/category1.svg',
                    color: Colors.redAccent,
                  ),
                  label: 'Новости'),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.star,
                    color: (tabsRouter.activeIndex == 1) ? Colors.redAccent : Colors.grey.shade700,
                  ),
                  label: 'Закладки'),
            ],
            onTap: (value) {
              tabsRouter.setActiveIndex(value);
            },
            currentIndex: tabsRouter.activeIndex,
          ),
        );
      },
    );
  }
}
