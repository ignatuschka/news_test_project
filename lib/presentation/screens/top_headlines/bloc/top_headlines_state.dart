import 'package:built_value/built_value.dart';
import 'package:news/domain/entity/articles_db_entity.dart';

part 'top_headlines_state.g.dart';

abstract class TopHeadlinesState implements Built<TopHeadlinesState, TopHeadlinesStateBuilder> {
  bool get isLoading;
  List<ArticlesDbEntity> get news;
  int get page;
  int get totalResults;

  TopHeadlinesState._();

  factory TopHeadlinesState([void Function(TopHeadlinesStateBuilder)? updates]) = _$TopHeadlinesState;

  factory TopHeadlinesState.initial() => TopHeadlinesState((b) => b
    ..isLoading = true
    ..news = []
    ..page = 1
    ..totalResults = 0);

  TopHeadlinesState setIsLoading(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  TopHeadlinesState setNews(List<ArticlesDbEntity> news) => rebuild((b) => (b)..news = news);
  
  TopHeadlinesState setPage(int page) => rebuild((b) => (b)..page = page);

  TopHeadlinesState setTotalResults(int totalResults) => rebuild((b) => (b)..totalResults = totalResults);
}
