import 'package:news/domain/entity/articles_db_entity.dart';

abstract class TopHeadlinesEvent {}

class TopHeadlinesInitialEvent extends TopHeadlinesEvent {}

class PaginationEvent extends TopHeadlinesEvent {}

class NavigateChosenNewsEvent extends TopHeadlinesEvent {
  final ArticlesDbEntity article;
  NavigateChosenNewsEvent({required this.article});
}

class InsertOrDeleteEvent extends TopHeadlinesEvent {
  final int index;
  InsertOrDeleteEvent({
    required this.index,
  });
}
