import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/usecase/all_articles_usecase.dart';
import 'package:news/domain/usecase/news_params.dart';
import 'package:news/domain/usecase/top_headlines_usecase.dart';
import 'package:news/presentation/screens/top_headlines/bloc/top_headlines_event.dart';
import 'package:news/presentation/screens/top_headlines/bloc/top_headlines_state.dart';

class TopHeadlinesBloc extends Bloc<TopHeadlinesEvent, TopHeadlinesState> {
  final TopHeadlinesUsecase topHeadlinesUsecase;
  final AllArticlesUsecase allArticlesUsecase;
  TopHeadlinesBloc({
    required this.topHeadlinesUsecase,
    required this.allArticlesUsecase,
  }) : super(TopHeadlinesState.initial()) {
    on<TopHeadlinesInitialEvent>(_initial);
    on<PaginationEvent>(_paginationEvent);
    add(TopHeadlinesInitialEvent());
  }

  Future<void> _initial(TopHeadlinesEvent event, Emitter<TopHeadlinesState> emit) async {
    emit(state.setIsLoading(true));
    final networkResult = await topHeadlinesUsecase(NewsParams(page: 1, pageSize: 15));
    final localResult = await allArticlesUsecase(EmptyParams());
    final articles = networkResult.articles.map((e) {
      if (localResult.contains(ArticlesDbEntity(
        title: e.title,
        description: e.description,
        urlToImage: e.urlToImage,
        favorited: true,
      ))) {
        final id = localResult
            .singleWhere((element) => (element ==
                ArticlesDbEntity(
                  title: e.title,
                  description: e.description,
                  urlToImage: e.urlToImage,
                  favorited: true,
                )))
            .id;
        return ArticlesDbEntity(
          id: id,
          title: e.title,
          description: e.description,
          urlToImage: e.urlToImage,
          favorited: true,
        );
      } else {
        return ArticlesDbEntity(
          title: e.title,
          description: e.description,
          urlToImage: e.urlToImage,
          favorited: false,
        );
      }
    }).toList();
    emit(state.setNews(articles).setIsLoading(false).setTotalResults(networkResult.totalResults));
  }

  Future<void> _paginationEvent(TopHeadlinesEvent event, Emitter<TopHeadlinesState> emit) async {
    if (state.totalResults / state.page > 14) {
      emit(state.setPage(state.page + 1));
      final networkResult = await topHeadlinesUsecase(NewsParams(page: state.page, pageSize: 15));
      final localResult = await allArticlesUsecase(EmptyParams());
      final articles = networkResult.articles.map((e) {
        if (localResult.contains(ArticlesDbEntity(
          title: e.title,
          description: e.description,
          urlToImage: e.urlToImage,
          favorited: true,
        ))) {
          return ArticlesDbEntity(
            id: localResult
                .singleWhere((element) =>
                    element ==
                    ArticlesDbEntity(
                      title: e.title,
                      description: e.description,
                      urlToImage: e.urlToImage,
                      favorited: true,
                    ))
                .id,
            title: e.title,
            description: e.description,
            urlToImage: e.urlToImage,
            favorited: true,
          );
        } else {
          return ArticlesDbEntity(
            title: e.title,
            description: e.description,
            urlToImage: e.urlToImage,
            favorited: false,
          );
        }
      }).toList();
      emit(state.setNews(state.news + articles));
    }
  }
}
