import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news/di/news_di.dart';
import 'package:news/presentation/screens/top_headlines/bloc/top_headlines_bloc.dart';
import 'package:news/presentation/screens/top_headlines/bloc/top_headlines_event.dart';
import 'package:news/presentation/screens/top_headlines/bloc/top_headlines_state.dart';

class TopHeadlinesScreen extends StatefulWidget {
  const TopHeadlinesScreen({super.key});

  @override
  State<TopHeadlinesScreen> createState() => _TopHeadlinesPage();
}

class _TopHeadlinesPage extends State<TopHeadlinesScreen> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_loadMoreData);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _loadMoreData() {
    if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
      context.read<TopHeadlinesBloc>().add(PaginationEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TopHeadlinesBloc, TopHeadlinesState>(
      bloc: context.read<TopHeadlinesBloc>(),
      builder: (context, state) {
        if (!state.isLoading) {
          return RefreshIndicator(
            onRefresh: () async => context.read<TopHeadlinesBloc>().add(TopHeadlinesInitialEvent()),
            child: ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              shrinkWrap: true,
              controller: _scrollController,
              itemCount: (state.news.length + 1 > state.totalResults) ? state.totalResults : state.news.length + 1,
              itemBuilder: (context, index) {
                if (index < state.news.length) {
                  return ArticleItem(article: state.news[index]);
                } else {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
