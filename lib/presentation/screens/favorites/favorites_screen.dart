import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news/di/news_di.dart';
import 'package:news/presentation/screens/favorites/bloc/favorites_bloc.dart';
import 'package:news/presentation/screens/favorites/bloc/favorites_event.dart';
import 'package:news/presentation/screens/favorites/bloc/favorites_state.dart';

class FavoritesScreen extends StatelessWidget {
  const FavoritesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Закладки',
          style: TextStyle(fontSize: 17),
        ),
        centerTitle: true,
      ),
      body: BlocBuilder<FavoritesBloc, FavoritesState>(
        builder: (context, state) {
          if (!state.isLoading) {
            if (state.articles.isNotEmpty) {
              return RefreshIndicator(
                onRefresh: () async => context.read<FavoritesBloc>().add(FavoritesInitialEvent()),
                child: ListView.builder(
                  physics: const AlwaysScrollableScrollPhysics(),
                  itemCount: state.articles.length,
                  itemBuilder: (context, index) {
                    return ArticleItem(article: state.articles[index]);
                  },
                ),
              );
            } else {
              return const Center(
                child: Text(
                  'Пусто',
                  style: TextStyle(fontSize: 32),
                ),
              );
            }
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
