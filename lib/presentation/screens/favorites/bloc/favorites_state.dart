import 'package:built_value/built_value.dart';
import 'package:news/domain/entity/articles_db_entity.dart';

part 'favorites_state.g.dart';

abstract class FavoritesState implements Built<FavoritesState, FavoritesStateBuilder> {
  bool get isLoading;
  List<ArticlesDbEntity> get articles;

  FavoritesState._();

  factory FavoritesState([void Function(FavoritesStateBuilder)? updates]) = _$FavoritesState;

  factory FavoritesState.initial() => FavoritesState((b) => b
    ..isLoading = true
    ..articles = []);

  FavoritesState setIsLoading(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  FavoritesState setArticles(List<ArticlesDbEntity> articles) => rebuild((b) => (b)..articles = articles);
}
