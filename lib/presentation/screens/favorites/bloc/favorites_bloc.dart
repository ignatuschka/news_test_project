// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/usecase/all_items_stream_usecase.dart';
import 'package:news/domain/usecase/news_params.dart';
import 'package:news/presentation/screens/favorites/bloc/favorites_event.dart';
import 'package:news/presentation/screens/favorites/bloc/favorites_state.dart';

class FavoritesBloc extends Bloc<FavoritesEvent, FavoritesState> {
  final AllItemsStreamUsecase allItemsStreamUsecase;
  FavoritesBloc({
    required this.allItemsStreamUsecase,
  }) : super(
          FavoritesState.initial(),
        ) {
    on<FavoritesInitialEvent>(_initial);
    add(FavoritesInitialEvent());
  }

  Future<void> _initial(FavoritesEvent event, Emitter<FavoritesState> emit) async {
    emit(state.setIsLoading(true));
    await emit.forEach<List<ArticlesDbEntity>>(
      allItemsStreamUsecase(EmptyParams()),
      onData: (data) => state.setArticles(data).setIsLoading(false),
      
    );
  }
}
