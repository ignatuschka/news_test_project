import 'package:built_value/built_value.dart';
import 'package:news/domain/entity/articles_db_entity.dart';

part 'chosen_news_state.g.dart';

abstract class ChosenNewsState implements Built<ChosenNewsState, ChosenNewsStateBuilder> {
  bool get isLoading;
  bool get isStarLoading;
  ArticlesDbEntity? get article;

  ChosenNewsState._();

  factory ChosenNewsState([void Function(ChosenNewsStateBuilder)? updates]) = _$ChosenNewsState;

  factory ChosenNewsState.initial() => ChosenNewsState((b) => b
    ..isLoading = true
    ..article = null
    ..isStarLoading = false);

  ChosenNewsState setIsLoading(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  ChosenNewsState setIsStarLoading(bool isStarLoading) => rebuild((b) => (b)..isStarLoading = isStarLoading);

  ChosenNewsState setArticle(ArticlesDbEntity? article) => rebuild((b) => (b)..article = article);
}
