// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/usecase/article_by_id_usecase.dart';
import 'package:news/domain/usecase/delete_article_by_id.dart';
import 'package:news/domain/usecase/insert_article_usecase.dart';
import 'package:news/navigation/news_navigator.dart';
import 'package:news/presentation/screens/chosen_news/bloc/chosen_news_event.dart';
import 'package:news/presentation/screens/chosen_news/bloc/chosen_news_state.dart';

class ChosenNewsBloc extends Bloc<ChosenNewsEvent, ChosenNewsState> {
  final NewsNavigator navigator;
  final InsertArticleUsecase insertArticleUsecase;
  final DeleteArticleByIdUsecase deleteArticleByIdUsecase;
  final ArticleByIdUsecase articleByIdUsecase;
  final ArticlesDbEntity article;
  ChosenNewsBloc({
    required this.navigator,
    required this.insertArticleUsecase,
    required this.deleteArticleByIdUsecase,
    required this.articleByIdUsecase,
    required this.article,
  }) : super(ChosenNewsState.initial()) {
    on<ChosenNewsInitialEvent>(_initial);
    on<InsertOrDeleteEvent>(_insertOrDeleteArticle);
    on<NavigateBackEvent>(_navigateBack);
    add(ChosenNewsInitialEvent());
  }

  Future<void> _initial(ChosenNewsEvent event, Emitter<ChosenNewsState> emit) async {
    emit(state.setIsLoading(true));
    final result = article.favorited
        ? await articleByIdUsecase(article.id!)
        : ArticlesDbEntity(
            title: article.title ?? 'Название новости',
            description: article.description ?? 'Описание новости',
            urlToImage: article.urlToImage ?? '',
            favorited: article.favorited,
          );
    emit(state.setArticle(result).setIsLoading(false));
  }

  Future<void> _insertOrDeleteArticle(ChosenNewsEvent event, Emitter<ChosenNewsState> emit) async {
    emit(state.setIsStarLoading(true));
    if (state.article!.favorited) {
      await deleteArticleByIdUsecase(state.article!.id!);
      emit(state
          .setArticle(ArticlesDbEntity(
            title: state.article?.title,
            description: state.article?.description,
            urlToImage: state.article?.urlToImage,
            favorited: false,
          ))
          .setIsStarLoading(false));
    } else {
      final id = await insertArticleUsecase(state.article!);
      emit(state
          .setArticle(ArticlesDbEntity(
            id: id,
            title: state.article?.title,
            description: state.article?.description,
            urlToImage: state.article?.urlToImage,
            favorited: true,
          ))
          .setIsStarLoading(false));
    }
  }

  void _navigateBack(ChosenNewsEvent event, Emitter<ChosenNewsState> emit) {
    navigator.navigateBack();
  }
}
