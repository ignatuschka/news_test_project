// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news/presentation/screens/chosen_news/bloc/chosen_news_bloc.dart';
import 'package:news/presentation/screens/chosen_news/bloc/chosen_news_event.dart';
import 'package:news/presentation/screens/chosen_news/bloc/chosen_news_state.dart';

class ChosenNewsScreen extends StatefulWidget {
  const ChosenNewsScreen({
    super.key,
  });

  @override
  State<ChosenNewsScreen> createState() => _ChosenNewsScreenState();
}

class _ChosenNewsScreenState extends State<ChosenNewsScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChosenNewsBloc, ChosenNewsState>(
      builder: (context, state) {
        if (!state.isLoading) {
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                onPressed: () {
                  context.read<ChosenNewsBloc>().add(NavigateBackEvent());
                },
                icon: const Icon(Icons.arrow_back),
              ),
              title: const Text('Назад'),
              actions: [
                IconButton(
                  onPressed: () => context.read<ChosenNewsBloc>().add(InsertOrDeleteEvent()),
                  icon: Icon(
                    Icons.star,
                    color: state.article!.favorited ? Colors.redAccent : Colors.grey.shade200,
                  ),
                ),
              ],
            ),
            body: Padding(
              padding: const EdgeInsets.only(top: 18, left: 16, right: 16),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    (state.article?.urlToImage != null)
                        ? CachedNetworkImage(
                            errorWidget: (context, url, error) => Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Image.asset(
                                'assets/images/errorImage.png',
                                height: 200,
                                width: 200,
                              ),
                            ),
                            imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            imageUrl: state.article!.urlToImage!,
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          )
                        : Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Image.asset(
                              'assets/images/errorImage.png',
                              height: 200,
                              width: 200,
                              fit: BoxFit.fill,
                            ),
                          ),
                    const SizedBox(height: 36),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          state.article?.title ?? 'Название новости',
                          style: const TextStyle(fontSize: 24),
                        ),
                        const SizedBox(height: 8),
                        Text(
                          state.article?.description ?? 'Описание новости',
                          style: TextStyle(fontSize: 20, color: Colors.grey.shade500),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
