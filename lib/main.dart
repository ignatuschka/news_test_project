import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:news/di/news_di.dart';
import 'package:news/navigation/app_router.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: Colors.transparent, systemNavigationBarColor: Colors.transparent));
  GetIt getIt = GetIt.I;
  NewsDIModule().updateInjections(getIt);
  runApp(MaterialApp.router(
    theme: ThemeData(
      useMaterial3: true,
      appBarTheme: const AppBarTheme(scrolledUnderElevation: 0),
      primaryColor: Colors.redAccent,
    ),
    routerDelegate: AutoRouterDelegate(getIt<AppRouter>()),
    routeInformationParser: getIt<AppRouter>().defaultRouteParser(),
  ));
}
