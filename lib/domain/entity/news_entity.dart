// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/foundation.dart';

import 'package:news/domain/entity/articles_entity.dart';

class NewsEntity {
  final String status;
  final int totalResults;
  final List<ArticlesEntity> articles; 
  const NewsEntity({
    required this.status,
    required this.totalResults,
    required this.articles,
  });

  @override
  bool operator ==(covariant NewsEntity other) {
    if (identical(this, other)) return true;
  
    return 
      other.status == status &&
      other.totalResults == totalResults &&
      listEquals(other.articles, articles);
  }

  @override
  int get hashCode => status.hashCode ^ totalResults.hashCode ^ articles.hashCode;
}
