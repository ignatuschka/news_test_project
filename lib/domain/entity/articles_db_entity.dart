class ArticlesDbEntity {
  final int? id;
  final String? title;
  final String? description;
  final String? urlToImage;
  final bool favorited;
  const ArticlesDbEntity({
    this.id,
    required this.title,
    required this.description,
    required this.urlToImage,
    required this.favorited,
  });

  @override
  bool operator ==(covariant ArticlesDbEntity other) {
    if (identical(this, other)) return true;
  
    return 
      other.title == title &&
      other.description == description &&
      other.urlToImage == urlToImage;
  }

  @override
  int get hashCode => title.hashCode ^ description.hashCode ^ urlToImage.hashCode;
}
