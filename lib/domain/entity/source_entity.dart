class SourceEntity {
  final String? id;
  final String name;
  const SourceEntity({
    this.id,
    required this.name,
  });

  @override
  bool operator ==(covariant SourceEntity other) {
    if (identical(this, other)) return true;

    return other.id == id && other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
