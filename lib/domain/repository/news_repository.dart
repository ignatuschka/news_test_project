import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/entity/news_entity.dart';

abstract class NewsRepository {
  Future<NewsEntity> getTopHeadlines({required int page, required int pageSize});

  Future<NewsEntity> getEverything({required int page, required int pageSize});

  Future<List<ArticlesDbEntity>> getAllAtricles();

  Future<int> insertAtricle({required ArticlesDbEntity entry});

  Stream<List<ArticlesDbEntity>> allItemsStream();

  Future<ArticlesDbEntity?> getAtricleById({required int id});

  Future<int> deleteAtricleById({required int id});
}