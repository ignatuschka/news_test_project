import 'package:news/core/base_usecase.dart';
import 'package:news/domain/entity/news_entity.dart';
import 'package:news/domain/repository/news_repository.dart';
import 'package:news/domain/usecase/news_params.dart';

class EverythingUsecase extends BaseUsecase<NewsEntity, NewsParams> {
  final NewsRepository newsRepository;
  EverythingUsecase({
    required this.newsRepository,
  });

  @override
  Future<NewsEntity> call(NewsParams params) async =>
      await newsRepository.getEverything(page: params.page, pageSize: params.pageSize);
}
