import 'package:news/core/base_usecase.dart';
import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/repository/news_repository.dart';

class ArticleByIdUsecase extends BaseUsecase<ArticlesDbEntity?, int> {
  final NewsRepository newsRepository;
  ArticleByIdUsecase({
    required this.newsRepository,
  });

  @override
  Future<ArticlesDbEntity?> call(int params) async => await newsRepository.getAtricleById(id: params);
}
