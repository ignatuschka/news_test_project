import 'package:news/core/base_usecase.dart';
import 'package:news/domain/entity/news_entity.dart';
import 'package:news/domain/repository/news_repository.dart';
import 'package:news/domain/usecase/news_params.dart';

class TopHeadlinesUsecase extends BaseUsecase<NewsEntity, NewsParams> {
  final NewsRepository newsRepository;
  TopHeadlinesUsecase({
    required this.newsRepository,
  });

  @override
  Future<NewsEntity> call(NewsParams params) async =>
      await newsRepository.getTopHeadlines(page: params.page, pageSize: params.pageSize);
}
