class NewsParams {
  final int page;
  final int pageSize;
  NewsParams({
    required this.page,
    required this.pageSize,
  });

  @override
  bool operator ==(covariant NewsParams other) {
    if (identical(this, other)) return true;

    return other.page == page && other.pageSize == pageSize;
  }

  @override
  int get hashCode => page.hashCode ^ pageSize.hashCode;
}

class EmptyParams {}
