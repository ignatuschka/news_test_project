import 'package:news/core/base_usecase.dart';
import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/repository/news_repository.dart';

class InsertArticleUsecase extends BaseUsecase<int, ArticlesDbEntity> {
  final NewsRepository newsRepository;
  InsertArticleUsecase({
    required this.newsRepository,
  });

  @override
  Future<int> call(ArticlesDbEntity params) async => await newsRepository.insertAtricle(entry: params);
}
