import 'package:news/core/base_usecase.dart';
import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/repository/news_repository.dart';
import 'package:news/domain/usecase/news_params.dart';

class AllArticlesUsecase extends BaseUsecase<List<ArticlesDbEntity>, EmptyParams> {
  final NewsRepository newsRepository;
  AllArticlesUsecase({
    required this.newsRepository,
  });

  @override
  Future<List<ArticlesDbEntity>> call(EmptyParams params) async => await newsRepository.getAllAtricles();
}
