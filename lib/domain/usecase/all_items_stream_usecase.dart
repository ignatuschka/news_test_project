import 'package:news/core/base_usecase.dart';
import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/repository/news_repository.dart';
import 'package:news/domain/usecase/news_params.dart';

class AllItemsStreamUsecase extends BaseStreamUsecase<List<ArticlesDbEntity>, EmptyParams> {
  final NewsRepository newsRepository;
  AllItemsStreamUsecase({
    required this.newsRepository,
  });

  @override
  Stream<List<ArticlesDbEntity>> call(EmptyParams params) => newsRepository.allItemsStream();
}
