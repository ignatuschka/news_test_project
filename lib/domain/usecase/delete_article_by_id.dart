import 'package:news/core/base_usecase.dart';
import 'package:news/domain/repository/news_repository.dart';

class DeleteArticleByIdUsecase extends BaseUsecase<int, int> {
  final NewsRepository newsRepository;
  DeleteArticleByIdUsecase({
    required this.newsRepository,
  });

  @override
  Future<int> call(int params) async => await newsRepository.deleteAtricleById(id: params);
}
