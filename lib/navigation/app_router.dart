import 'package:auto_route/auto_route.dart';
import 'package:flutter/widgets.dart';
import 'package:news/di/news_di.dart';
import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/presentation/screens/main_screen.dart';
import 'package:news/presentation/screens/main_tab_screen.dart';

part 'app_router.gr.dart';

const everythingPage = 'everythingPage';
const topHeadlinesPage = 'topHeadlinesPage';
const favoritesPage = 'favoritesPage';
const chosenNewsPage = 'chosenNewsPage';
const mainPage = 'RootMainPage';
const mainTabPage = 'mainTabPage';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: [
    AutoRoute(
      page: MainPage,
      initial: true,
      children: [
        AutoRoute(
          path: mainTabPage,
          page: MainTabPage,
          children: [
            AutoRoute(
              path: topHeadlinesPage,
              page: TopHeadlinesPage,
            ),
            AutoRoute(
              path: everythingPage,
              page: EverythingPage,
            ),
          ],
        ),
        AutoRoute(
          path: favoritesPage,
          page: FavoritesPage,
        ),
      ],
    ),
    AutoRoute(
      path: chosenNewsPage,
      page: ChosenNewsPage,
    ),
  ],
)
class AppRouter extends _$AppRouter {}
