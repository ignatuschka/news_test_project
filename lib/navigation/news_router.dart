import 'package:auto_route/auto_route.dart';
import 'package:auto_route/empty_router_widgets.dart';
import 'package:news/di/news_di.dart';
import 'package:news/presentation/screens/main_tab_screen.dart';

class NewsRouter {
  static const root = 'RootNews';
  static const router = AutoRoute(
    path: root,
    name: root,
    page: EmptyRouterPage,
    children: [
      AutoRoute(
        path: mainPage,
        page: MainTabPage,
        initial: true,
      ),
      AutoRoute(
        path: everythingPage,
        page: EverythingPage,
      ),
      AutoRoute(
        path: topHeadlinesPage,
        page: TopHeadlinesPage,
      ),
      AutoRoute(
        path: favoritesPage,
        page: FavoritesPage,
      ),
      AutoRoute(
        path: chosenNewsPage,
        page: ChosenNewsPage,
      ),
    ],
  );
  static const everythingPage = 'everythingPage';
  static const topHeadlinesPage = 'topHeadlinesPage';
  static const favoritesPage = 'favoritesPage';
  static const chosenNewsPage = 'chosenNewsPage';
  static const mainPage = 'mainPage';
}
