import 'package:get_it/get_it.dart';
import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/navigation/app_router.dart';

GetIt getIt = GetIt.instance;

abstract class NewsNavigator {
  Future<void> navigateToChosenNews({required ArticlesDbEntity article});

  void navigateBack();
}

class NewsNavigatorImpl implements NewsNavigator {
  static final AppRouter _ar = getIt<AppRouter>();

  @override
  void navigateBack() {
    _ar.pop();
  }

  @override
  Future<void> navigateToChosenNews({required ArticlesDbEntity article}) async {
    _ar.push(ChosenNewsRoute(article: article));
  }
}
