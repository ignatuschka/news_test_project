// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:news/core/db_tables_extantion.dart';
import 'package:news/data/data_sources/news_local_data_source.dart';
import 'package:news/data/data_sources/news_remote_data_source.dart';
import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/entity/news_entity.dart';
import 'package:news/domain/repository/news_repository.dart';

class NewsRepositoryImpl extends NewsRepository {
  final NewsRemoteDataSource newsRemoteDataSource;
  final NewsLocalDataSource newsLocalDataSource;
  NewsRepositoryImpl({
    required this.newsRemoteDataSource,
    required this.newsLocalDataSource,
  });

  @override
  Future<NewsEntity> getEverything({required int page, required int pageSize}) async {
    final result = await newsRemoteDataSource.getEverything(page: page, pageSize: pageSize);
    return result.toEntity();
  }

  @override
  Future<NewsEntity> getTopHeadlines({required int page, required int pageSize}) async {
    final result = await newsRemoteDataSource.getTopHeadlines(page: page, pageSize: pageSize);
    return result.toEntity();
  }

  @override
  Stream<List<ArticlesDbEntity>> allItemsStream() =>
      newsLocalDataSource.allItemsStream().map((event) => event.map((e) => e.toEntity()).toList());

  @override
  Future<int> deleteAtricleById({required int id}) async => newsLocalDataSource.deleteAtricleById(id: id);

  @override
  Future<List<ArticlesDbEntity>> getAllAtricles() async {
    final result = await newsLocalDataSource.getAllAtricles();
    return result.map((e) => e.toEntity()).toList();
  }

  @override
  Future<ArticlesDbEntity?> getAtricleById({required int id}) async {
    final result = await newsLocalDataSource.getAtricleById(id: id);
    return result?.toEntity();
  }

  @override
  Future<int> insertAtricle({required ArticlesDbEntity entry}) async =>
      await newsLocalDataSource.insertAtricle(entry: entry.toTable());
}
