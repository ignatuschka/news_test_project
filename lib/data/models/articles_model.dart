// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:json_annotation/json_annotation.dart';
import 'package:news/core/string_extention.dart';
import 'package:news/data/models/source_model.dart';
import 'package:news/domain/entity/articles_entity.dart';

part 'articles_model.g.dart';

@JsonSerializable(createToJson: false)
class ArticlesModel {
  final SourceModel? source;
  final String? author;
  final String? title;
  final String? description;
  final String? url;
  final String? urlToImage;
  final String? publishedAt;
  final String? content;
  const ArticlesModel({
    required this.source,
    required this.author,
    required this.title,
    required this.description,
    required this.url,
    required this.urlToImage,
    required this.publishedAt,
    required this.content,
  });

  factory ArticlesModel.fromJson(Map<String, dynamic> json) => _$ArticlesModelFromJson(json);

  ArticlesEntity toEntity() => ArticlesEntity(
        source: source?.toEntity(),
        author: author,
        title: title,
        description: description,
        url: url,
        urlToImage: urlToImage,
        publishedAt: publishedAt?.isoToDateTime(),
        content: content,
      );
}
