import 'package:json_annotation/json_annotation.dart';

part 'rates_model.g.dart';


@JsonSerializable(createToJson: false)
class Rates {
  double? cNY;

  Rates({
    this.cNY,
  });

  factory Rates.fromJson(Map<String, dynamic> json) => _$RatesFromJson(json);
}
