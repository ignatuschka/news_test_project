import 'package:json_annotation/json_annotation.dart';
import 'package:news/domain/entity/source_entity.dart';

part 'source_model.g.dart';

@JsonSerializable(createToJson: false)
class SourceModel {
  final String? id;
  final String name;
  const SourceModel({
    this.id,
    required this.name,
  });

  factory SourceModel.fromJson(Map<String, dynamic> json) => _$SourceModelFromJson(json);

  SourceEntity toEntity() => SourceEntity(id: id, name: name);
}
