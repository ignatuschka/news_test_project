import 'package:json_annotation/json_annotation.dart';

import 'package:news/data/models/articles_model.dart';
import 'package:news/domain/entity/news_entity.dart';

part 'news_model.g.dart';

@JsonSerializable(createToJson: false)
class NewsModel {
  final String status;
  final int totalResults;
  final List<ArticlesModel> articles;
  const NewsModel({
    required this.status,
    required this.totalResults,
    required this.articles,
  });

  factory NewsModel.fromJson(Map<String, dynamic> json) => _$NewsModelFromJson(json);

  NewsEntity toEntity() => NewsEntity(
        status: status,
        totalResults: totalResults,
        articles: articles.map((e) => e.toEntity()).toList(),
      );
}
