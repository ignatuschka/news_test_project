import 'package:dio/dio.dart';
import 'package:news/data/models/news_model.dart';
import 'package:retrofit/retrofit.dart';

part 'news_api.g.dart';

@RestApi(baseUrl: 'https://newsapi.org/v2/')
abstract class NewsApi {
  factory NewsApi(Dio dio, {String baseUrl}) = _NewsApi;

  @GET('/everything?q=tesla&page={page}&pageSize={pageSize}&apiKey=a44d89f4dba848b6a93d294c6105fc8f')
  Future<NewsModel> getEverything(@Path('page') int page, @Path('pageSize') int pageSize);

  @GET(
      '/top-headlines?country=us&category=business&page={page}&pageSize={pageSize}&apiKey=a44d89f4dba848b6a93d294c6105fc8f')
  Future<NewsModel> getTopHeadlines(@Path('page') int page, @Path('pageSize') int pageSize);
}
