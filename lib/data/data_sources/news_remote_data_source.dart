// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:news/data/models/news_model.dart';
import 'package:news/data/network/news_api.dart';

abstract class NewsRemoteDataSource {
  Future<NewsModel> getEverything({required int page, required int pageSize});

  Future<NewsModel> getTopHeadlines({required int page, required int pageSize});
}

class NewsRemoteDataSourceImpl extends NewsRemoteDataSource {
  final NewsApi newsApi;
  NewsRemoteDataSourceImpl({required this.newsApi});

  @override
  Future<NewsModel> getEverything({required int page, required int pageSize}) async =>
      await newsApi.getEverything(page, pageSize);

  @override
  Future<NewsModel> getTopHeadlines({required int page, required int pageSize}) async =>
      await newsApi.getTopHeadlines(page, pageSize);
}
