// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:news/data/db/daos/articles_dao.dart';
import 'package:news/data/db/database.dart';

abstract class NewsLocalDataSource {
  Future<List<ArticlesTableCompanion>> getAllAtricles();

  Future<int> insertAtricle({required ArticlesTableCompanion entry});

  Stream<List<ArticlesTableCompanion>> allItemsStream();

  Future<ArticlesTableCompanion?> getAtricleById({required int id});

  Future<int> deleteAtricleById({required int id});
}

class NewsLocalDataSourceImpl extends NewsLocalDataSource {
  final ArticlesDao dao;
  NewsLocalDataSourceImpl({
    required this.dao,
  });

  @override
  Stream<List<ArticlesTableCompanion>> allItemsStream() => dao.allItemsStream();

  @override
  Future<int> deleteAtricleById({required int id}) async => await dao.deleteAtricleById(id: id);

  @override
  Future<List<ArticlesTableCompanion>> getAllAtricles() async => await dao.getAllAtricles();

  @override
  Future<ArticlesTableCompanion?> getAtricleById({required int id}) async => await dao.getAtricleById(id: id);

  @override
  Future<int> insertAtricle({required ArticlesTableCompanion entry}) async => dao.insertAtricle(entry: entry);
}
