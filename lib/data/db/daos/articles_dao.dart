import 'package:drift/drift.dart';
import 'package:news/data/db/database.dart';
import 'package:news/data/db/tables/articles_database_model.dart';

part 'articles_dao.g.dart';

@DriftAccessor(tables: [ArticlesTable])
class ArticlesDao extends DatabaseAccessor<AppDb> with _$ArticlesDaoMixin {
  ArticlesDao(AppDb db) : super(db);

  Stream<List<ArticlesTableCompanion>> allItemsStream() =>
      select(articlesTable).map((e) => e.toCompanion(false)).watch();

  Future<int> insertAtricle({required ArticlesTableCompanion entry}) async => await into(articlesTable).insert(entry);

  Future<List<ArticlesTableCompanion>> getAllAtricles() async {
    final items = await select(articlesTable).get();
    final result = items.map((e) => e.toCompanion(false)).toList();
    return result;
  }

  Future<ArticlesTableCompanion?> getAtricleById({required int id}) async {
    final item = await (select(articlesTable)..where((tbl) => tbl.id.equals(id))).getSingleOrNull();
    final result = item?.toCompanion(false);
    return result;
  }

  Future<int> deleteAtricleById({required int id}) async =>
      await (delete(articlesTable)..where((tbl) => tbl.id.equals(id))).go();
}
