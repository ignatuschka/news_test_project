import 'package:drift/drift.dart';

class ArticlesTable extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get title => text()();

  TextColumn get description => text()();

  TextColumn get urlToImage => text()();

  BoolColumn get favorited => boolean()();
}
