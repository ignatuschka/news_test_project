// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import 'package:news/core/dio_module.dart';
import 'package:news/data/data_sources/news_local_data_source.dart';
import 'package:news/data/data_sources/news_remote_data_source.dart';
import 'package:news/data/db/daos/articles_dao.dart';
import 'package:news/data/db/database.dart';
import 'package:news/data/network/news_api.dart';
import 'package:news/data/repository/news_repository_impl.dart';
import 'package:news/domain/entity/articles_db_entity.dart';
import 'package:news/domain/repository/news_repository.dart';
import 'package:news/domain/usecase/all_articles_usecase.dart';
import 'package:news/domain/usecase/all_items_stream_usecase.dart';
import 'package:news/domain/usecase/article_by_id_usecase.dart';
import 'package:news/domain/usecase/delete_article_by_id.dart';
import 'package:news/domain/usecase/everything_usecase.dart';
import 'package:news/domain/usecase/insert_article_usecase.dart';
import 'package:news/domain/usecase/top_headlines_usecase.dart';
import 'package:news/navigation/app_router.dart';
import 'package:news/navigation/news_navigator.dart';
import 'package:news/presentation/screens/chosen_news/bloc/chosen_news_bloc.dart';
import 'package:news/presentation/screens/chosen_news/chosen_news_screen.dart';
import 'package:news/presentation/screens/everything/bloc/everything_bloc.dart';
import 'package:news/presentation/screens/everything/everything_screen.dart';
import 'package:news/presentation/screens/favorites/bloc/favorites_bloc.dart';
import 'package:news/presentation/screens/favorites/favorites_screen.dart';
import 'package:news/presentation/screens/top_headlines/bloc/top_headlines_bloc.dart';
import 'package:news/presentation/screens/top_headlines/top_headlines_screen.dart';
import 'package:news/presentation/widgets/article_list_item/article_list_item.dart';
import 'package:news/presentation/widgets/article_list_item/bloc/article_list_item_bloc.dart';

class NewsDIModule {
  void updateInjections(GetIt instance) {
    instance.registerLazySingleton<NewsRemoteDataSource>(() => NewsRemoteDataSourceImpl(newsApi: instance.get()));

    instance.registerLazySingleton<NewsLocalDataSource>(() => NewsLocalDataSourceImpl(dao: instance.get()));

    instance.registerLazySingleton<NewsRepository>(() => NewsRepositoryImpl(
          newsRemoteDataSource: instance.get(),
          newsLocalDataSource: instance.get(),
        ));

    instance.registerFactory(() => TopHeadlinesUsecase(newsRepository: instance.get()));

    instance.registerFactory(() => EverythingUsecase(newsRepository: instance.get()));

    instance.registerFactory(() => AllArticlesUsecase(newsRepository: instance.get()));

    instance.registerFactory(() => AllItemsStreamUsecase(newsRepository: instance.get()));

    instance.registerFactory(() => ArticleByIdUsecase(newsRepository: instance.get()));

    instance.registerFactory(() => DeleteArticleByIdUsecase(newsRepository: instance.get()));

    instance.registerFactory(() => InsertArticleUsecase(newsRepository: instance.get()));

    instance.registerSingleton<Dio>(DioModule.provideDio());

    instance.registerLazySingleton(() => NewsApi(instance.get()));

    instance.registerSingleton<AppRouter>(AppRouter());

    instance.registerSingleton<NewsNavigator>(NewsNavigatorImpl());

    instance.registerSingleton<AppDb>(AppDb());

    instance.registerLazySingleton(() => ArticlesDao(instance.get()));
  }
}

class EverythingPage extends StatelessWidget {
  const EverythingPage({super.key});

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => EverythingBloc(
        everythingUsecase: getIt(),
        allArticlesUsecase: getIt(),
      ),
      child: const EverythingScreen(),
    );
  }
}

class TopHeadlinesPage extends StatelessWidget {
  const TopHeadlinesPage({super.key});

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TopHeadlinesBloc(
        topHeadlinesUsecase: getIt(),
        allArticlesUsecase: getIt(),
      ),
      child: const TopHeadlinesScreen(),
    );
  }
}

class FavoritesPage extends StatelessWidget {
  const FavoritesPage({super.key});

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FavoritesBloc(
        allItemsStreamUsecase: getIt(),
      ),
      child: const FavoritesScreen(),
    );
  }
}

class ChosenNewsPage extends StatelessWidget {
  final ArticlesDbEntity article;
  const ChosenNewsPage({
    Key? key,
    required this.article,
  }) : super(key: key);

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ChosenNewsBloc(
        navigator: getIt(),
        insertArticleUsecase: getIt(),
        deleteArticleByIdUsecase: getIt(),
        articleByIdUsecase: getIt(),
        article: article,
      ),
      child: const ChosenNewsScreen(),
    );
  }
}

class ArticleItem extends StatelessWidget {
  final ArticlesDbEntity article;
  const ArticleItem({
    Key? key,
    required this.article,
  }) : super(key: key);

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ArticleListItemBloc(
        navigator: getIt(),
        insertArticleUsecase: getIt(),
        deleteArticleByIdUsecase: getIt(),
        article: article,
      ),
      child: const ArticleListItem(),
    );
  }
}
