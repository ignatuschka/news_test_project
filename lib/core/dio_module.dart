import 'package:dio/dio.dart';

abstract class DioModule {
  static Dio provideDio() {
    final dio = Dio();

    dio
          ..options.headers = {
            'Content-Type': 'application/json; charset=utf-8'
          }
          ..interceptors.add(LogInterceptor(
            request: true,
            responseBody: true,
            requestBody: true,
            requestHeader: true,
          ))
        // use interceptors if needed
        /*..interceptors.add(
        InterceptorsWrapper(),
      )*/
        ;

    return dio;
  }
}