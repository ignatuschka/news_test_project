abstract class BaseUsecase<Type, Params> {
  Future<Type> call(Params params);
}

abstract class BaseStreamUsecase<Type, Params> {
  Stream<Type> call(Params params);
}
