import 'package:drift/drift.dart';
import 'package:news/data/db/database.dart';
import 'package:news/domain/entity/articles_db_entity.dart';

extension DbEntityExtantion on ArticlesDbEntity {
  ArticlesTableCompanion toTable() => ArticlesTableCompanion(
        title: Value(title!),
        description: Value(description!),
        urlToImage: Value(urlToImage!),
        favorited: const Value(true),
      );
}

extension DbTablesExtantion on ArticlesTableCompanion {
  ArticlesDbEntity toEntity() => ArticlesDbEntity(
        id: id.value,
        title: title.value,
        description: description.value,
        urlToImage: urlToImage.value,
        favorited: favorited.value,
      );
}
